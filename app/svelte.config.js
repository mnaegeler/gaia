const preprocess = require('svelte-preprocess');

const config = {
  compilerOptions: {},
  preprocess: [
    preprocess(),
  ],
}

module.exports = config;
