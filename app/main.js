import "@babel/polyfill";
import App from "./App.svelte";
import "@fortawesome/fontawesome-free/css/fontawesome.css";
import "@fortawesome/fontawesome-free/css/regular.css";
import "@fortawesome/fontawesome-free/css/solid.css";
import "./styles/styles.scss";
import { post } from "./utils/request";

window.onunhandledrejection = function (e) {
  if (e.reason.toString().match(/Network Error/) !== null) {
    // No internet, no need to log...
    return;
  }

  console.log("got rejection", e);
  console.log("current params:", {
    CurrentView: window.CurrentView,
    CurrentItems: window.CurrentItems,
    CurrentItemData: window.CurrentItemData,
    CurrentMetadata: window.CurrentMetadata,
    CurrentOrganizationId: window.CurrentOrganizationId,
  });
  
  const viewParsed = {
    id: window.CurrentView.id,
    key: window.CurrentView.key,
    labelKey: window.CurrentView.labelKey,
    slug: window.CurrentView.slug,
    type: window.CurrentView.type,
  };

  const data = ["*New unhandled exception:*"];
  data.push("\n**Error:** ");
  data.push(e.reason);
  data.push("```" + e.reason.stack + "```");

  data.push("\n**Organization ID:** `");
  data.push(window.CurrentOrganizationId);
  data.push("`");

  data.push("\n**User ID:** `");
  data.push(window.CurrentMetadata?.user?.id);
  data.push("`");

  data.push("\n**Role ID:** `");
  data.push(window.CurrentMetadata?.user?.RoleId);
  data.push("`");

  data.push("\n**Current View:** ```");
  data.push(JSON.stringify(viewParsed, null, 2));
  data.push("```");

  post("/bug-report", { data: data.join("") });
};

const app = new App({
  target: document.body,
});

export default app;
