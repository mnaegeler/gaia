import axios from "axios";
import { authToken, currentOrganization } from "../store";

let token = null;
authToken.subscribe((value) => {
  token = value;
});

let $currentOrganization = null;
currentOrganization.subscribe((value) => {
  $currentOrganization = value;
});

let host = "";

if (import.meta.env.DEV) {
  host = "http://localhost:3000";
}
// if (process.env.GITPOD_WORKSPACE_URL) {
//   host = process.env.GITPOD_WORKSPACE_URL.replace('https://', 'https://3000-')
// } else if (process.env.NODE_ENV === 'development') {
//   host = 'http://localhost:3000'
// }

function makeLinkPath(url, data) {
  data["authorization"] = `Bearer ${token}`;
  const dataString = Object.keys(data)
    .map((key) => `${key}=${data[key]}`)
    .join("&");
  return [buildURL(url), dataString].join("?");
}

function buildURL(url) {
  if (url.startsWith("/") === false) {
    url = `/${url}`;
  }
  return `${host}/api${url}`;
}

function get(
  url,
  data,
  headers = {},
  shouldAwaitToken = true,
  cancelToken = null
) {
  return new Promise((resolve, reject) => {
    function makeReq() {
      axios
        .get(buildURL(url), {
          params: data,
          headers: {
            ...headers,
            Authorization: `Bearer ${token}`,
            Organization: `${$currentOrganization || ""}`,
          },
          cancelToken,
        })
        .then(resolve)
        .catch(reject);
    }

    if (shouldAwaitToken === true) {
      awaitToken().then(makeReq);
    } else {
      makeReq();
    }
  });
}

function post(
  url,
  data,
  headers = {},
  shouldAwaitToken = true,
  cancelToken = null
) {
  return new Promise((resolve, reject) => {
    function makeReq() {
      axios
        .post(buildURL(url), data, {
          headers: {
            ...headers,
            Authorization: `Bearer ${token}`,
            Organization: `${$currentOrganization || ""}`,
          },
          cancelToken,
        })
        .then(resolve)
        .catch(reject);
    }

    if (shouldAwaitToken === true) {
      awaitToken().then(makeReq);
    } else {
      makeReq();
    }
  });
}

function put(
  url,
  data,
  headers = {},
  shouldAwaitToken = true,
  cancelToken = null
) {
  return new Promise((resolve, reject) => {
    function makeReq() {
      axios
        .put(buildURL(url), data, {
          headers: {
            ...headers,
            Authorization: `Bearer ${token}`,
            Organization: `${$currentOrganization || ""}`,
          },
          cancelToken,
        })
        .then(resolve)
        .catch(reject);
    }

    if (shouldAwaitToken === true) {
      awaitToken().then(makeReq);
    } else {
      makeReq();
    }
  });
}

function destroy(
  url,
  data,
  headers = {},
  shouldAwaitToken = true,
  cancelToken = null
) {
  return new Promise((resolve, reject) => {
    function makeReq() {
      axios
        .delete(buildURL(url), {
          params: data,
          headers: {
            ...headers,
            Authorization: `Bearer ${token}`,
            Organization: `${$currentOrganization || ""}`,
          },
          cancelToken,
        })
        .then(resolve)
        .catch(reject);
    }

    if (shouldAwaitToken === true) {
      awaitToken().then(makeReq);
    } else {
      makeReq();
    }
  });
}

function awaitToken() {
  return new Promise((resolve) => {
    let timer = setInterval(() => {
      if (token !== null) {
        clearInterval(timer);
        resolve();
      }
    }, 5);
  });
}

export { get, post, put, destroy, makeLinkPath };
