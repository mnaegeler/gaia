import { labels } from "../store";

let $labels = {};

labels.subscribe((value) => ($labels = value));

function loadDropdownTriggers() {
  const handler = ($event) => {
    const closest = $event.target.closest(".dropdown");
    const open = document.querySelectorAll(".dropdown");

    open.forEach((dropdown) => {
      if (dropdown !== closest && dropdown.classList.contains("is-active")) {
        dropdown.querySelector(".dropdown-trigger").click();
      }
    });
  };

  document.removeEventListener("click", handler);
  document.addEventListener("click", handler);
}

function getLabel(labelKey, suppressWarn = false, replacements=null) {
  if (labelKey && typeof labelKey !== "string") {
    labelKey = labelKey.labelKey;
  }

  if (suppressWarn === false && (!$labels || !$labels[labelKey])) {
    // console.warn(`Label not found. Label key: ${labelKey}`)
  }

  let resultLabel = ($labels && $labels[labelKey]) || labelKey;
  if (replacements) {
    for (let key in replacements) {
      resultLabel = resultLabel.replace(new RegExp(`#${key}`, 'g'), replacements[key]);
    }
  }

  return resultLabel;
}

function getPlaceholder(key, fallbackLabel = true, suppressWarn = false) {
  if (key && typeof key !== "string") {
    if (!key.placeholder) {
      key = fallbackLabel === true ? key.labelKey : "...";
    } else {
      key = key.placeholder;
    }
  }

  if (key === "...") {
    return key;
  }

  if (suppressWarn === false && !$labels[key]) {
    // console.warn(`Label not found. Label key: ${key}`)
  }

  return $labels[key] || key;
}

function getFilterLabel(filterField) {
  if (filterField.labelKey) {
    return getLabel(filterField);
  }

  return getLabel(filterField.ViewField);
}

function buildURL(url) {
  let host = "";

  if (Array.isArray(url)) {
    url = url[0];
  }

  if (!url) {
    return "";
  }

  const isURL = url.startsWith("http");
  const isBase64 = url.startsWith("data:");
  if (!isURL && import.meta.env.DEV && !isBase64) {
    host = "http://localhost:3001";
  }

  return `${host}${url}`;
}

function crop(string, length) {
  return string ? string.replace(new RegExp(`^(.{0,${length}}).*$`), "$1") : "";
}

function getInitials(name) {
  return name
    .split(" ")
    .map((word) => word[0])
    .join("")
    .toUpperCase();
}

function convertTime(time) {
  if (time !== undefined && time > 0) {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;

    const hours = Math.floor(time / 3600);
    time = time - hours * 3600;

    return (
      str_pad_left(hours, "0", 2) +
      ":" +
      str_pad_left(minutes, "0", 2) +
      ":" +
      str_pad_left(seconds, "0", 2)
    );
  }
  return "hh:mm:ss";
}

function str_pad_left(string, pad, length) {
  return (new Array(length + 1).join(pad) + string).slice(-length);
}

function formatDateTime(value, showTime = false) {
  const date = new Date(value);
  if (date !== "Invalid Date") {
    let options;
    if (showTime === false) {
      options = { day: "numeric", month: "numeric", year: "numeric" };
    }

    return date.toLocaleString("pt-BR", options);
  }

  return date;
}

function formatDecimal(value, field) {
  const options = {};
  if (field && field.format === "currency") {
    options.style = "currency";
    options.currency = "BRL";
  }

  return ((value || 0) * 1).toLocaleString("pt-br", options);
}

function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}

export {
  getLabel,
  getPlaceholder,
  getFilterLabel,
  buildURL,
  crop,
  getInitials,
  convertTime,
  formatDateTime,
  formatDecimal,
  loadDropdownTriggers,
  debounce,
};
