psql -U gitpod database_development -c 'DROP SCHEMA public CASCADE; CREATE SCHEMA public; GRANT ALL ON SCHEMA public TO gitpod; GRANT ALL ON SCHEMA public TO public;'
psql -U gitpod database_development -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
node src/generators/syncDatabase.js
yarn sequelize db:seed:all
