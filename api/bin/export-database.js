require('dotenv').config()

const { promisify } = require('util')
const fs = require('fs').promises
const exec = promisify(require('child_process').exec)
const path = require('path')

const models = require('../src/app/models')

process.env.PGHOST = process.env.DB_HOST
process.env.PGDATABASE = process.env.DB_NAME
process.env.PGUSER = process.env.DB_USER
process.env.PGPASSWORD = process.env.DB_PASSWORD

async function exportDatabase () {
  const outputDir = path.resolve(__dirname, '..', 'database_dumps')
  try {
    await fs.mkdir(outputDir)
  } catch (e) {}

  const DBModels = Object.keys(models).filter(n => n.toLowerCase() !== 'sequelize')
  const tables = DBModels.map(model => models[model].getTableName())

  tables.forEach(async table => {
    try {
      let date = (new Date()).toISOString()
      date = date.replace(/^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}).*/, '$1')
      date = date.replace(/\D/g, '')
      const file = path.resolve(outputDir, `${date}-${table}.json`)
      const res = await exec(`psql -c 'SELECT array_to_json(array_agg(\"${table}\")) AS ok_json FROM \"${table}\"' -t -o ${file}`)
      console.log(res)
    } catch (e) {
      console.error(e)
    }
  })
}

exportDatabase()
