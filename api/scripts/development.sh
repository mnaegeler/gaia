#!/bin/bash

NODE_ENV=development nodemon server.js --trace-warning --ignore __tests --ignore src/app/models --ignore src/app/metadata
