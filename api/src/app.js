require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env",
});

const cors = require("cors");
const express = require("express");
const path = require("path");
// TODO: change express to fastify (for perf)

const AppController = function () {
  const app = express();

  if (process.env.NODE_ENV === "development") {
    app.use(cors());
  }

  app.use(express.json({ limit: "500mb" }));
  app.use("/public", express.static("public"));
  app.use("/", express.static("public/dist"));

  // app.get("/", (req, res) =>
  //   res.sendFile(path.join(__dirname, "../../app/dist/index.html"))
  // );

  app.use(require("./routes"));

  function getApp() {
    return app;
  }

  return {
    getApp,
  };
};

module.exports = AppController().getApp();
