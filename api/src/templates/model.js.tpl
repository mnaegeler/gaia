module.exports = (sequelize, DataTypes) => {
  const {#name} = sequelize.define('{#name}', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal('uuid_generate_v4()'),
    },
    {#fields}
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
    },
  });

  {#name}.associate = (models) => {
    {#relations}
    {#name}.belongsTo(models.User, { as: 'CreatedByUser', constraints: false });
  };

  return {#name};
}
