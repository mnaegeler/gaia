const models = require("../models");
const { Model, ModelField } = models;

// function camelToSnake (str) {
//   return str.replace(/[A-Z]/g, (letter, index) => `${index > 0 ? '_' : ''}${letter.toLowerCase()}`)
// }

module.exports = {
  async getModelMetadata(modelName) {
    return await Model.findOne({
      where: {
        name: modelName,
      },
      include: ["ModelFields", "ModelValidations"],
    });
  },

  async getVisibleFields(ModelId, role) {
    return await ModelField.findAll({
      where: {
        ModelId,
      },
      include: [
        {
          model: models["ModelFieldPermission"],
          as: "ModelFieldPermissions",
          required: !role.hasFullAccess,
          where: {
            RoleId: role.id,
          },
        },
      ],
    });
  },

  async getTargetModelByAliasAndSource(targetModelAlias, sourceModelId) {
    const modelField = await ModelField.findOne({
      where: { targetModelAlias, ModelId: sourceModelId },
      include: { model: ModelField, as: 'TargetModelField' },
    });
    const model = await Model.findOne({
      where: {
        name: modelField.targetModel,
      },
    });

    return {
      model,
      modelField,
    };
  },
};
