function startOfDay(date) {
    return new Date(date.toISOString().replace(/^(.*)T\d{2}:\d{2}:\d{2}[.\d{3}]*Z$/, '$1T00:00:00-03:00'));
}

function endOfDay(date) {
    return new Date(date.toISOString().replace(/^(.*)T\d{2}:\d{2}:\d{2}[.\d{3}]*Z$/, '$1T23:59:59-03:00'));
}

module.exports = {
    startOfDay,
    endOfDay,
};
