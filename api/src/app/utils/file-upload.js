const fs = require('fs').promises
const path = require('path')
const { v4: uuidv4 } = require('uuid')

const publicUploadFolder = path.resolve(__dirname, '../../..', 'public', 'uploads')
const publicUploadPath = '/public/uploads'

async function uploadFiles (metadata, body) {
  const bodyKeys = Object.keys(body)
  for (let index = 0; index < bodyKeys.length; index++) {
    const key = bodyKeys[index]
    const metaField = metadata.ModelFields.find(m => m.name === key)

    if (metaField && metaField.type === 'file') {
      const isUpload = /^data:([a-zA-Z-]+)\/([a-zA-Z-._]+);base64,(.*)$/.exec(body[key])
      if (!isUpload) {
        // Check if it's not propositally null
        if (body[key] !== null) {
          // Prevent erasing picture when it's not a picture
          delete body[key]
        }

        continue
      }

      let extension = isUpload[2]
      // console.log({ extension });
      if (extension === 'x-zip' || extension === 'zip' || extension === 'x-zip-compressed' || extension === 'zip-compressed') {
        extension = 'zip';
      } else if (extension === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        extension = 'xlsx';
      } else if (extension === 'msexcel') {
        extension = 'xls';
      } else if (extension === 'octet-stream' || extension === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
        extension = 'docx';
      } else if (extension === 'msword') {
        extension = 'doc';
      }

      const data = isUpload[3]

      if (data) {
        const name = uuidv4()
        const filename = `${name}.${extension}`
        const filepath = `${publicUploadFolder}/${filename}`
        const fileToSave = `${publicUploadPath}/${filename}`

        body[key] = fileToSave

        try {
          let buffer = Buffer.from(data, 'base64')
          await fs.mkdir(publicUploadFolder, { recursive: true })
          await fs.writeFile(filepath, buffer)
        } catch (e) {
          throw new Error(e)
        }
      }
    }
  }
} 

module.exports = {
  uploadFiles
}
