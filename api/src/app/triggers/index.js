const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);

// TODO: Implement bulk operations
const singleTriggers = [
  "beforeCreate",
  "afterCreate",
  "beforeDestroy",
  "afterDestroy",
  "beforeRestore",
  "afterRestore",
  "beforeUpdate",
  "afterUpdate",
];

const multiTriggers = [
  "beforeBulkCreate",
  "afterBulkCreate",
  "beforeBulkDestroy",
  "afterBulkDestroy",
  "beforeBulkRestore",
  "afterBulkRestore",
  "beforeBulkUpdate",
  "afterBulkUpdate",
];

const availableTriggers = [
  ...singleTriggers,
  ...multiTriggers,
];

function getTriggers() {
  const triggerFiles = {};
  fs.readdirSync(__dirname)
    .filter((file) => {
      return (
        file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
      );
    })
    .forEach((file) => {
      const filename = file.replace(/(.*)Trigger\.js/, "$1");
      triggerFiles[filename] = require(path.join(__dirname, file));
    });

  const triggers = {};
  availableTriggers.forEach((triggerName) => {
    triggers[triggerName] = async function (...restParams) {
      let instance, options, error;
      if (restParams.length > 2) {
        instance = restParams[0];
        options = restParams[1];
        error = restParams[2];
      } else {
        instance = restParams[0];
        options = restParams[1];
      }

      const modelName = instance.constructor.name;

      const userFunctionToCall = triggerName.replace('Bulk', '');
      const data = {
        new: [],
        old: [],
        options,
        error,
      };

      console.log(triggerName, modelName);
      if (singleTriggers.includes(triggerName)) {
        data.new = [ instance.dataValues ];
        data.old = [ instance._previousDataValues ];
      } else {
        data.new = instance;
      }
      
      if (
        triggerFiles[modelName] &&
        typeof triggerFiles[modelName][userFunctionToCall] === "function"
      ) {
        await triggerFiles[modelName][userFunctionToCall](data);
      }
    };
  });

  return triggers;
}

module.exports = getTriggers();
