const cache = require("../../config/cache");
const { exportModelMetadata } = require("../../generators/export");
const { writeModelsToFiles } = require("../../generators/models");
const { synchronizeDatabase } = require("../../generators/sync-database");

function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = {
  async afterCreate(instances, options) {
    // Can't import before because of recursion (models.js imports config.js that imports models.js)
    let models = require("../models");
    for (let instance of instances.new) {
      const model = await models["Model"].findByPk(instance.ModelId);
      const modelName = model.name;

      await exportModelMetadata(modelName);
      await writeModelsToFiles();
      await synchronizeDatabase(modelName);
    }

    cache.emit('clearMetadata');
  },

  async afterUpdate(instance, options) {
    const models = require("../models");
    for (let instance of instances.new) {
      const model = await models.Model.findByPk(instance.ModelId);
      const modelName = model.name;

      await exportModelMetadata(modelName);
      await writeModelsToFiles();
      await synchronizeDatabase(modelName);
    }

    cache.emit('clearMetadata');
  },
};
