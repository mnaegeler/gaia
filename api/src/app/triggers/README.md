# Triggers
Triggers são os gatilhos usados na nossa aplicação para execução de códigos customizados antes ou depois de determinadas ações do sistema.
Hoje estão disponíveis triggers para criação, edição, remoção e restauração de registros.
