module.exports = {
  async afterCreate(instance, options) {
    let models = require("../models");
    const user = await models["User"].findByPk(instance.TargetUserId);

    if (user.receiveEmailNotifications) {
      const mailMessage = `Você foi mencionado: ${instance.link}`;

      const mailOptions = {
        to: user.email,
        subject: "Nova menção",
        text: mailMessage,
      };

      Mail.send(mailOptions)
        .catch(e => {
          Log.create({
            function: 'NotificationTrigger',
            method:'afterCreate',
            modelName: 'Notification',
            query: { error: JSON.stringify(e) },
          });
        });
    }
  }
}
