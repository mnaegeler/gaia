const cache = require("../../config/cache");

module.exports = {
  async afterCreate(instance) {
    cache.emit("clearMetadata");
  },
  async afterUpdate(instance) {
    cache.emit("clearMetadata");
  },
};
