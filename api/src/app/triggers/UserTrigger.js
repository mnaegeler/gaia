const Mail = require("../utils/mail");

module.exports = {
  async beforeCreate(instance, options) {
    const { User } = require("../models");
    const emailAlreadyExists = await User.findOne({ where: { email: instance.email } });
    if (emailAlreadyExists) {
      throw new Error('error/user-already-exists');
    }
  },
  
  async afterCreate(instance, options) {
    const user = instance;
    const { UserPasswordToken, Log } = require("../models");
    const token = await UserPasswordToken.create(
      { UserId: user.id },
      { returning: ["id"] }
    );

    const url = `${process.env.APP_URL}#/set-password`;
    const mailMessage = `Para ${
      user.passwordHash ? "redefinir" : "criar"
    } sua senha, acesse o link abaixo: ${url}/${token.id}`;

    const mailOptions = {
      to: user.email,
      subject: user.passwordHash ? "Redefinir senha" : "Criar senha",
      text: mailMessage,
    };

    Mail.send(mailOptions)
      .catch(e => {
        Log.create({
          function: 'UserTrigger',
          method:'afterCreate',
          modelName: 'User',
          query: { error: JSON.stringify(e) },
        });
      });
  },
};
