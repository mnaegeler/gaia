const { User, Role, Language } = require("../models");

const func = function () {
  let _user = null;
  let _isSessionEmulated = null;

  const user = () => _user;
  const isSessionEmulated = () => _isSessionEmulated;

  const loadUser = async (userId, sessionEmulated = null) => {
    if (userId) {
      _user = await User.findByPk(userId, { include: [Role, Language, "PhotoFile"] });
      _isSessionEmulated = sessionEmulated;
    }
  };

  return {
    user,
    loadUser,
    isSessionEmulated,
  };
};

module.exports = func();
