const { Op } = require('sequelize')

const { Model, ModelPermission } = require('../models')

module.exports = {
  async hasPermission (modelName, action, user) {
    return await Model.findOne({
      where: {
        name: modelName
      },

      include: {
        model: ModelPermission,
        as: 'ModelPermissions',
        required: !user.Role.hasFullAccess,
        where: {
          action,
          RoleId: {
            [Op.eq]: user.Role.id,
          },
        },
      }
    })
  }
}
