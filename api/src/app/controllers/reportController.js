const reportConfig = require("../../config/reports");
const Sequelize = require("sequelize");
const models = require("../models");

const controller = {
  async getAvailableReports( req, res) {
    const reports = Object.values(reportConfig).map((r, index) => {
      return {
        label: r.label,
        viewType: r.viewType,
        id: Object.keys(reportConfig).at(index),
      }
    });

    return res.json({ result: reports });
  },
  
  async runReport(req, res) {
    const config = reportConfig[req.params.id];
    const reportMode = req.query.mode || "summary"; // 'summary' | 'table' | 'summary|table'
    const orgId = req.get("Organization");

    if (!orgId) {
      return res.status(404).json({ message: "Organization not found" });
    }

    if (!config || !config.model || !models[config.model]) {
      return res.status(404).json({ message: "Report or Model not found" });
    }

    const result = {
      summary: null,
      data: null,
    };

    if (reportMode.includes("summary")) {
      const summaryData = await models[config.model].findAll({
        include: (config.include || []).map((i) => ({
          model: models[i],
          as: i,
          attributes: [],
        })),

        attributes: [
          // Main grouping function select
          [ Sequelize.literal(config.summary.grouper), "grouper" ],

          // Summarized field
          [
            Sequelize.literal(config.summary.summarizedAttribute),
            "summarized",
          ],

          // All other fields to select
          ...(config.summary.columns || []).map((c) => [
            Sequelize.literal(c),
            c.replace(".", "_"),
          ]),
        ],

        where: {
          OrganizationId: req.get("Organization"),

          // Main date filter
          [Sequelize.Op.and]: [
            Sequelize.literal(
              `DATE_TRUNC('month', "${config.intervalFilterField.replace(
                ".",
                '"."'
              )}") > NOW() - INTERVAL '${config.intervalFilterValue}'`
            ),
            config.filters && Sequelize.literal(config.filters),
          ],
        },

        order: [ [ Sequelize.literal(config.summary.grouper), "ASC" ] ],
        group: [
          // Main grouping function
          Sequelize.literal(config.summary.grouper),
          // All other fields to group
          ...(config.summary.columns || []).map((c) => [Sequelize.col(c), c]),
        ],
      });

      result.summary = summaryData;
    }

    if (reportMode.includes("table")) {
      const fullData = await models[config.model].findAll({
        include: (config.include || []).map((i) => ({
          model: models[i],
          as: i,
          attributes: [],
        })),

        separate: true,

        attributes: [
          // All other fields to select
          ...(config.data.columns || []).map((c) => [
            Sequelize.literal(c),
            c.replace(".", "_"),
          ]),
        ],

        where: {
          OrganizationId: req.get("Organization"),

          // Main date filter
          [Sequelize.Op.and]: [
            Sequelize.literal(
              `DATE_TRUNC('month', "${config.intervalFilterField.replace(
                ".",
                '"."'
              )}") > NOW() - INTERVAL '${config.intervalFilterValue}'`
            ),
            config.filters && Sequelize.literal(config.filters),
          ],
        },

        limit: 2000,

        order: [ [ Sequelize.literal(config.summary.grouper), "ASC" ] ],
      });

      console.log({ fullData_first: JSON.stringify(fullData[0]) })
      result.data = fullData;
    }

    return res.status(200).json({
      result,
      config: {
        model: config.model,
        viewType: config.viewType,
        label: config.label,
        summary_columns: config.summary.columns,
        data_columns: config.data.columns,
        intervalFilterValue: config.intervalFilterValue,
      },
    });
  },
};

module.exports = controller;
