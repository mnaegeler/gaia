const { Op } = require("sequelize");
const { JSDOM } = require("jsdom");
const { makeValidations } = require("../validation-rules");
const Auth = require("../facades/Auth");
const Permission = require("../facades/Permission");
const {
  getModelMetadata,
  getVisibleFields,
  getTargetModelByAliasAndSource,
} = require("../utils/metadata");

const update = require("./rest/update");
const create = require("./rest/create");
const deleteRecord = require("./rest/delete");

let models = require("../models");
const { Sequelize, sequelize } = require("../models");
const { getLabel } = require("../utils/labels");
const { Model } = models;

function logError(msg) {
  console.log("\x1b[31m%s\x1b[0m", `(!) ${msg.message}`, msg);
}

function prepareFiltering(modelMeta, variableReplacements) {
  async function buildFilterQuery(clause) {
    if (
      clause.customFunction &&
      variableReplacements[clause.customFunction] !== undefined
    ) {
      return variableReplacements[clause.customFunction]();
    }

    if (!clause.name || !clause.value) {
      return;
    }

    const value = variableReplacements[clause.value]
      ? variableReplacements[clause.value]
      : clause.value;

    const isRelationship = clause.name.includes(".");
    if (!isRelationship) {
      return {
        [clause.name]: { [Op[`${clause.criteria}`]]: value },
      };
    }

    if (isRelationship) {
      let model = modelMeta;
      let modelField;
      let baseScript;

      const nameParts = clause.name.replace(/[#$]/g, "").split(".");
      const lastIndex = nameParts.length - 1;
      const joins = [];
      let lastWhere;
      let operator = "=";
      if (clause.criteria === "neq") {
        operator = "!=";
      } else if (clause.criteria === "gte") {
        operator = ">=";
      } else if (clause.criteria === "lte") {
        operator = "<=";
      } else if (clause.criteria === "gt") {
        operator = ">";
      } else if (clause.criteria === "lt") {
        operator = "<";
      }

      for (let index = 0; index < nameParts.length; index++) {
        const node = nameParts[index];

        if (index < lastIndex) {
          const target = await getTargetModelByAliasAndSource(node, model.id);

          const relatedModel = target.model;
          modelField = target.modelField;

          const relatedTableName = models[relatedModel.name].tableName;

          if (index === 0) {
            let idComparative = "";
            if (modelField.type === "belongsTo") {
              idComparative = `AND "${model.name}"."${modelField.name}" = ${modelField.targetModelAlias}."id"`;
            } else {
              idComparative = `AND "${
                modelField.TargetModelField?.targetModel ||
                modelField.targetModel
              }"."id" = ${modelField.targetModelAlias}."${
                modelField.TargetModelField?.name || modelField.name
              }"`;
            }

            baseScript = `(EXISTS(
                SELECT ${modelField.targetModelAlias}."id"
                FROM "${relatedTableName}" AS ${modelField.targetModelAlias}
                ##JOINS##
                WHERE
                ${modelField.targetModelAlias}."deletedAt" IS NULL 
                ${idComparative}
                ##WHERE##
              ))`.replace(/[\n\s]{1,}/g, " ");
          } else {
            joins.push(
              ` INNER JOIN "${relatedTableName}" ${
                modelField.targetModelAlias
              } ON ${modelField.targetModelAlias}."${
                modelField.TargetModelField?.name || modelField.name
              }" = ${
                modelField.TargetModelField?.targetModel ||
                modelField.targetModel
              }.id AND ${modelField.targetModelAlias}."deletedAt" IS NULL `
            );
          }
          model = relatedModel;
        } else {
          lastWhere = ` AND ${modelField.targetModelAlias}."${node}" ${operator} '${value}' `;
        }
      }

      baseScript = baseScript
        .replace(/\#\#WHERE\#\#/, lastWhere)
        .replace(/\#\#JOINS\#\#/, joins.join(" "));

      return sequelize.literal(baseScript);
    }
  }

  async function buildLogicalTree(filters) {
    if (Array.isArray(filters)) {
      return await Promise.all(filters.map(buildFilterQuery));
    }

    const f = {};
    if (filters["OR"]) {
      f[Op.or] = await buildLogicalTree(filters["OR"]);
    }

    if (filters["AND"]) {
      f[Op.and] = await buildLogicalTree(filters["AND"]);
    }

    return f;
  }

  return {
    buildLogicalTree,
  };
}

const magicController = {
  async browse(req, res) {
    models = require("../models");
    const user = Auth.user();

    const modelName = req.get("Model");

    let debugInfo = [];
    let page = req.query.page;
    const includeQuery = req.query.include || [];
    let order = req.query.order;
    let limitQuery = req.query.limit;
    const queriedAttributes = req.query.attributes;

    const modelMeta = await getModelMetadata(modelName);

    let include = [];

    const variableReplacements = {
      _SessionUserId_: user.id,
      _SessionUserRoleHasFullAccess_: () =>
        Sequelize.literal(`1=${user.Role.hasFullAccess ? 1 : 0}`),
    };

    if (!modelName || !models[modelName]) {
      return res.status(400).send({ messages: ["Model not provided"] });
    }

    const hasPermission = await Permission.hasPermission(
      modelName,
      "browse",
      user
    );

    if (!hasPermission) {
      return res
        .status(403)
        .send({ messages: ["Forbidden", modelName, "browse", user] });
    }

    let modelMetaFiltering = {};
    const prepareFilters = prepareFiltering(modelMeta, variableReplacements);

    if (modelMeta.filters) {
      modelMetaFiltering = await prepareFilters.buildLogicalTree(
        modelMeta.filters
      );
    }

    let clientClauses = {};
    let clientIncludes = [];
    let parsedClientClauses;

    // verifica se existe OrganizationId na query
    const shouldForceCurrentOrganization =
      !(typeof req.query.where === "string") ||
      !req.query.where.includes('"OrganizationId"');

    try {
      parsedClientClauses = JSON.parse(req.query.where);
      debugInfo.push({ isString, parsedClientClauses });
    } catch (e) {}

    let clientFiltering = {};
    if (parsedClientClauses) {
      clientFiltering = await prepareFilters.buildLogicalTree(
        parsedClientClauses
      );
    }

    const quickSearch = {};

    if (
      parsedClientClauses &&
      parsedClientClauses.OR &&
      parsedClientClauses.OR.length
    ) {
      quickSearch[Op.or] = parsedClientClauses.OR.map((clause) => {
        const key = clause.name.includes(".")
          ? `\$${clause.name}\$`
          : clause.name;
        return {
          [key]: { [Op[`${clause.criteria}`]]: clause.value },
        };
      });
    }

    if (!Array.isArray(clientFiltering)) {
      if (!clientFiltering[Op.and] || !clientFiltering[Op.and].length) {
        delete clientFiltering[Op.and];
      }

      if (!clientFiltering[Op.or] || !clientFiltering[Op.or].length) {
        delete clientFiltering[Op.or];
      }

      if (!clientFiltering[Op.or] && !clientFiltering[Op.and]) {
        clientFiltering = undefined;
      }
    }

    let where = {
      [Op.and]: [modelMetaFiltering, clientFiltering],
      ...quickSearch,
    };

    if (
      clientClauses &&
      clientClauses[Op.and] &&
      Object.values(clientClauses[Op.and]).length
    ) {
      where[Op.and].push(Object.values(clientClauses[Op.and]));
    }

    // Verifica se o model selecionado possui o campo OrganizationId, aí realiza a atribuição do organization do usuario
    if (
      modelMeta.ModelFields.find((field) => field.name === "OrganizationId")
    ) {
      if (!where) {
        where = {};
      }

      if (!where["OrganizationId"] && shouldForceCurrentOrganization) {
        where["OrganizationId"] = { [Op[`eq`]]: req.get("Organization") };
      }
    }

    const viewIncludes = includeQuery
      .map((i) => {
        const newInclude = {
          model: null,
          as: null,
          required: false,
          // duplicating: false,
        };

        try {
          // object
          const parsed = JSON.parse(i);
          newInclude.model = models[parsed.model];
          newInclude.as = parsed.as;
          if (parsed.required) {
            newInclude.required = true;
          }

          if (parsed.include) {
            newInclude.include = parsed.include;
          }
        } catch (e) {
          // string
          const field = modelMeta.ModelFields.find(
            (mf) => mf.targetModelAlias === i
          );
          if (field) {
            newInclude.model = models[field.targetModel];
            newInclude.as = field.targetModelAlias;
          }
        }

        return newInclude;
      })
      .filter((i) => !!i);

    include.push({
      model: models["User"],
      as: "CreatedByUser",
      attributes: ["id", "name", "PhotoFileId"],
      include: ["PhotoFile"],
    });

    include = [...include, ...clientIncludes, ...viewIncludes];

    if (order && order.length) {
      try {
        order[0] = JSON.parse(order[0]);
      } catch (e) {}
    } else {
      order = [["updatedAt", "DESC"]];
    }

    const filterAttribute = (attr) => {
      return (
        queriedAttributes === undefined ||
        (queriedAttributes !== undefined && queriedAttributes.includes(attr))
      );
    };

    // only the fields that are visible
    const attributes = (await getVisibleFields(modelMeta.id, user.Role))
      .filter((a) => a.type !== "hasMany")
      .map((a) => a.name)
      .filter(filterAttribute);

    try {
      const shouldLimitQuery = !["ModelField", "ViewField"].includes(modelName);
      const limit = shouldLimitQuery
        ? limitQuery && limitQuery <= 100
          ? limitQuery
          : 100
        : undefined;
      page = page ? Number(page) : 1;
      const offset = shouldLimitQuery
        ? page === 1
          ? 0
          : limit * (page - 1)
        : undefined;
      const result = await models[modelName].findAll({
        attributes: ["id", "updatedAt", ...attributes],
        include,
        where,
        order,
        limit,
        offset,
      });

      //  make this work
      // const totalItems = await models[modelName].count({ attributes: ['id'], where, include, group: 'Model.id' });

      return res.status(200).send({ result, debugInfo });
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error: error.message, debugInfo });
    }
  },

  async show(req, res) {
    models = require("../models");
    const id = req.params.id;
    const modelName = req.get("Model");
    let include = req.query.include;
    const user = Auth.user();
    const debugInfo = [];
    const queriedAttributes = req.query.attributes;

    if (!modelName) {
      return res.status(400).send({ messages: ["Model not provided"] });
    }

    const hasPermission = await Permission.hasPermission(
      modelName,
      "show",
      user
    );

    if (!hasPermission) {
      return res
        .status(403)
        .send({ messages: ["Forbidden", modelName, "show", user] });
    }

    if (!include) {
      include = [];
    }

    const orderModels = [];
    const variableReplacements = {
      _SessionUserId_: user.id,
      _SessionUserRoleHasFullAccess_: () =>
        Sequelize.literal(`1=${user.Role.hasFullAccess ? 1 : 0}`),
    };

    // verifica se existe OrganizationId na query
    const shouldForceCurrentOrganization =
      !(typeof req.query.where === "string") ||
      !req.query.where.includes('"OrganizationId"');

    const modelMeta = await getModelMetadata(modelName);

    const metas = {};
    const visibilities = {};
    for (let index = 0; index < include.length; index++) {
      let includeData = include[index];
      try {
        includeData = JSON.parse(include[index]);
        if (includeData.model) {
          let includeModelMeta = await getModelMetadata(includeData.model);
          if (!includeModelMeta) {
            const { model } = await getTargetModelByAliasAndSource(
              includeData.as,
              modelMeta.id
            );

            includeModelMeta = model;
          }
          metas[includeData.model] = includeModelMeta;
          visibilities[includeData.model] = (
            await getVisibleFields(metas[includeData.model].id, user.Role)
          )
            .filter((a) => a.type !== "hasMany")
            .map((a) => a.name);
        }
      } catch (e) {}

      if (typeof includeData === "string") {
        metas[includeData] = await getModelMetadata(includeData);
        if (!metas[includeData]) {
          const { model } = await getTargetModelByAliasAndSource(
            includeData,
            modelMeta.id
          );

          metas[includeData] = model;
        }
        visibilities[includeData] = (
          await getVisibleFields(metas[includeData].id, user.Role)
        )
          .filter((a) => a.type !== "hasMany")
          .map((a) => a.name);
      }
    }

    include = include.map((i) => {
      try {
        i = JSON.parse(i);
        const name = i.model;
        i.model = models[name];

        if (i.required === undefined) {
          i.required = false;
        }

        if (visibilities[name]) {
          i.attributes = ["id", ...visibilities[name]];
        }

        if (i.where) {
          Object.keys(i.where).forEach((key) => {
            if (i.where[key] && i.where[key].replace) {
              i.where[key] = i.where[key].replace(
                new RegExp(`${i.where[key]}`, "gi"),
                variableReplacements[i.where[key]]
              );
            }
          });
        }

        if (!i.order) {
          i.order = ["createdAt"];
        } else if (i.order === "order") {
          orderModels.push([name, "order"]);
        }
      } catch (e) {}

      if (typeof i === "string") {
        let model = i;
        if (models[model] === undefined) {
          model = metas[model].name;
        }

        const includeObj = {
          model: models[model],
          as: i,
        };

        if (visibilities[i]) {
          includeObj.attributes = ["id", ...visibilities[i]];
        }

        return includeObj;
      }
      return i;
    });

    include.push({
      model: models["User"],
      as: "CreatedByUser",
      attributes: ["id", "name"],
    });

    let modelMetaFiltering = {};
    if (modelMeta.filters) {
      const prepareFilters = prepareFiltering(modelMeta, variableReplacements);
      modelMetaFiltering = await prepareFilters.buildLogicalTree(
        modelMeta.filters
      );
      debugInfo.push({ modelMetaFiltering });
    }

    // const where = builtWhereClauses.where;
    const where = {
      [Op.and]: [modelMetaFiltering, { id }],
    };

    // Verifica se o model selecionado possui o campo OrganizationId, aí realiza a atribuição do organization do usuario
    if (
      modelMeta.ModelFields.find((field) => field.name === "OrganizationId")
    ) {
      if (!where) {
        where = {};
      }

      if (!where["OrganizationId"] && shouldForceCurrentOrganization) {
        where["OrganizationId"] = { [Op[`eq`]]: req.get("Organization") };
      }
    }

    let result = null;
    try {
      const filterAttribute = (attr) => {
        return (
          queriedAttributes === undefined ||
          (queriedAttributes !== undefined && queriedAttributes.includes(attr))
        );
      };

      // only the fields that are visible
      const attributes = (await getVisibleFields(modelMeta.id, user.Role))
        .filter((a) => a.type !== "hasMany")
        .map((a) => a.name)
        .filter(filterAttribute);

      result = await models[modelName].findOne({
        attributes: ["id", "updatedAt", ...attributes],
        include,
        where,
      });

      if (orderModels.length) {
        orderModels.forEach((i) => {
          result[`${i[0]}s`] = result[`${i[0]}s`].sort((a, b) => {
            if (a[i[1]] > b[i[1]]) {
              return 1;
            }

            if (a[i[1]] < b[i[1]]) {
              return -1;
            }

            return 0;
          });
        });
      }
    } catch (e) {
      console.error(e);
    }

    if (!result) {
      return res.status(404).send({ messages: [`${modelName} not found`] });
    }

    return res.status(200).send({ result, debugInfo });
  },

  async store(req, res) {
    models = require("../models");
    const user = Auth.user();

    const modelName = req.get("Model");
    const body = req.body;

    if (!modelName) {
      return res.status(400).send({ messages: ["Model not provided"] });
    }

    const hasPermission = await Permission.hasPermission(
      modelName,
      "store",
      user
    );
    if (!hasPermission) {
      return res
        .status(403)
        .send({ messages: ["Forbidden", modelName, "store", user] });
    }

    const modelMeta = await getModelMetadata(modelName);

    let element = null;
    try {
      const sessionUser = Auth.user();
      const userId = sessionUser.id;
      body.CreatedByUserId = userId;

      // CreatedByUser Organization
      if (
        !body.OrganizationId &&
        modelMeta.ModelFields.findIndex((f) => f.name === "OrganizationId") > -1
      ) {
        body.OrganizationId = req.get("Organization");
      }

      // User autofill
      if (
        modelMeta.ModelFields.findIndex((f) => f.name === "UserId") > -1 &&
        !body.UserId
      ) {
        body.UserId = userId;
      }

      // Default Organization
      if (
        modelMeta.ModelFields.findIndex(
          (f) => f.name === "DefaultOrganizationId"
        ) > -1
      ) {
        body.DefaultOrganizationId = req.get("Organization");
      }

      if (
        modelName === "User" &&
        (!body.OrganizationUsers || !body.OrganizationUsers.length)
      ) {
        body.OrganizationUsers = [{ OrganizationId: req.get("Organization") }];
      }

      try {
        await makeValidations(modelMeta, body, user);
      } catch (e) {
        logError(e);
        return res.status(500).send({ messages: [e.message] });
      }

      const bodyToCreate = { ...body };
      const bodyKeys = Object.keys(bodyToCreate);

      for (let bodyIndex = 0; bodyIndex < bodyKeys.length; bodyIndex++) {
        const key = bodyKeys[bodyIndex];
        const metaField = modelMeta.ModelFields.find((m) => m.name === key);
        if (
          metaField &&
          (metaField.type === "integer" || metaField.type === "decimal")
        ) {
          if (bodyToCreate[key] === "") {
            bodyToCreate[key] = null;
          }
        }
      }

      element = await models[modelName].create(bodyToCreate, {
        returning: ["*"],
      });

      // Auto-wire the record with the owner (the logged-in user)
      for (let bodyIndex = 0; bodyIndex < bodyKeys.length; bodyIndex++) {
        const key = bodyKeys[bodyIndex];
        const metaField = modelMeta.ModelFields.find((m) => m.name === key);

        // TODO: remove this "Mentions" code from here, put on a separate module.
        if (metaField && metaField.type === "richText") {
          // Test if someone is mentioned in rich text...
          const hasMentions = bodyToCreate[key].match(
            /data-denotation-char="@" data-id="([a-zA-Z0-9-]+)"/g
          );
          if (hasMentions && hasMentions.length) {
            const modelId = modelMeta.id;
            const recordId = element.id;

            let mentionContent = "notification/mention";
            const replacements = {
              name: sessionUser.name,
              recordName: "",
              comment: element.comment,
            };

            let modelToLink = modelId;
            let recordToLink = recordId;
            if (modelName === "Comment") {
              mentionContent = "notification/mention-comment";

              modelToLink = element.ModelId;
              recordToLink = element.recordId;

              // Get the record name - by default "name" is used
              // TODO: add a description field to model to get which field should be used here
              const model = await models["Model"].findByPk(modelToLink);
              const record = await models[model.name].findByPk(recordToLink);
              replacements.recordName = record.name;

              // extract plain text from comment
              const dom = new JSDOM(
                `<!DOCTYPE html><div>${element.comment}</div>`
              );
              let div = dom.window.document.querySelector("div");
              const mentions = Array.from(div.querySelectorAll(".mention"));
              for (let mention of mentions) {
                mention.dataset.value = "";
              }

              div = dom.window.document.querySelector("div");
              replacements.comment = div.textContent.trim().slice(0, 29);
            }

            const view = await models["View"].findOne({
              where: { ModelId: modelToLink, type: "ShowView" },
            });
            let link = "";
            if (view) {
              link = `${process.env.APP_URL}/#${view.slug}/${recordToLink}`;
            }

            const notificationsToCreate = [];
            for (let mention of hasMentions) {
              const targetUserId = mention.replace(
                /.*data-id="([a-zA-Z0-9-]+)".*/,
                "$1"
              );
              const user = await models["User"].findByPk(targetUserId, {
                include: [models["Language"]],
              });

              const content = await getLabel(
                user.Language.abbr,
                mentionContent,
                "",
                replacements
              );

              notificationsToCreate.push({
                read: false,
                TargetUserId: user.id,
                type: "mention",
                content,
                link,
                OriginModelId: modelId,
                originRecordId: recordId,
                OrganizationId: req.get("Organization"),
                CreatedByUserId: userId,
              });
            }

            try {
              await models["Notification"].bulkCreate(notificationsToCreate);
            } catch (e) {
              console.error(e);
            }
          }
        }

        // hasMany
        if (
          Array.isArray(bodyToCreate[key]) === true &&
          metaField.type === "hasMany"
        ) {
          if (modelName === "Model" && key === "ModelFields") {
            bodyToCreate[key].push({
              name: "createdAt",
              type: "datetime",
              modelFieldType: "DataTypes.DATE",
            });
            bodyToCreate[key].push({
              name: "updatedAt",
              type: "datetime",
              modelFieldType: "DataTypes.DATE",
            });
            bodyToCreate[key].push({
              name: "deletedAt",
              type: "datetime",
              modelFieldType: "DataTypes.DATE",
            });
            bodyToCreate[key].push({
              name: "CreatedByUserId",
              type: "belongsTo",
              targetModel: "User",
              targetModelAlias: "CreatedByUser",
            });
            bodyToCreate[key].push({
              name: "OrganizationId",
              type: "belongsTo",
              targetModel: "Organization",
              targetModelAlias: "Organization",
            });
          }

          const childItems = bodyToCreate[key];
          for (
            let childIndex = 0;
            childIndex < childItems.length;
            childIndex++
          ) {
            const arrayItem = childItems[childIndex];
            arrayItem.CreatedByUserId = userId;

            let item = await models[metaField.targetModel].upsert(
              { ...arrayItem },
              { returning: ["*"] }
            );
            await element[`add${key}`](item[0]);
            bodyToCreate[key][childIndex].id = item[0].id;
          }
        }
      }
    } catch (e) {
      logError(e);
      return res.status(500).send({ messages: [e.message] });
    }

    return res.status(200).send({ id: element.id });
  },

  async update(req, res) {
    models = require("../models");
    const user = Auth.user();

    const id = req.params.id;
    const modelName = req.get("Model");
    const body = req.body;

    if (!modelName) {
      return res.status(400).send({ messages: ["Model not provided"] });
    }

    const hasPermission = await Permission.hasPermission(
      modelName,
      "update",
      user
    );
    if (!hasPermission) {
      return res
        .status(403)
        .send({ messages: ["Forbidden", modelName, "update", user] });
    }

    let element = null;
    try {
      element = await models[modelName].findByPk(id);
    } catch (e) {
      console.error(e);
    }

    if (!element) {
      const model = await Model.findOne({ where: { name: modelName } });

      return res.status(404).send({ messages: [`${model.name} not found`] });
    }

    const userId = user.id;
    const modelMeta = await getModelMetadata(modelName);

    try {
      await makeValidations(modelMeta, body, user, id);
    } catch (e) {
      logError(e);
      return res.status(500).send({ messages: [e.message] });
    }

    const bodyToUpdate = { ...body };
    Object.keys(bodyToUpdate).forEach(async (key) => {
      const metaField = modelMeta.ModelFields.find((m) => m.name === key);

      // hasMany
      if (
        Array.isArray(bodyToUpdate[key]) === true &&
        metaField.type === "hasMany"
      ) {
        let relationsToRemove = await element[`get${key}`]();

        try {
          bodyToUpdate[key] = bodyToUpdate[key].map(async (arrayItem) => {
            if (arrayItem.id) {
              // If the relation still exists, remove from removal list
              const index = relationsToRemove.findIndex(
                (r) => r.id === arrayItem.id
              );
              if (index !== -1) {
                relationsToRemove.splice(index, 1);
              }
            }

            arrayItem.CreatedByUserId = userId;

            let item = await models[metaField.targetModel].upsert(
              { ...arrayItem },
              { returning: ["*"] }
            );
            await element[`add${key}`](item[0]);
            return item[0];
          });
        } catch (e) {
          throw new Error(e);
        }

        relationsToRemove.forEach(async (rel) => {
          await rel.destroy();
        });
      }
    });

    try {
      await element.update(body);
    } catch (e) {
      logError(e);
      return res.status(500).send({ messages: [e.message] });
    }

    return res.status(200).send({ id: element.id });
  },

  async delete(req, res) {
    const user = Auth.user();
    const modelName = req.get("Model");

    try {
      const deletedId = await deleteRecord(user, modelName, req.params.id);

      console.log({ deletedId });
      return res.status(200).send(deletedId);
    } catch (e) {
      let code;
      if (e === 'error/model-not-provided') {
        code = 400;
      } else if (e === 'error/permission-denied') {
        code = 403;
      } else if (e === 'error/record-not-found') {
        code = 400;
      } else {
        code = 500;
      }

      if (e instanceof Error) {
        e = e.message;
      }

      return res.status(code).send({ messages: [ e ] });
    }
  },

  async createV2(req, res) {
    const user = Auth.user();
    const modelName = req.get("Model");

    try {
      const createdIds = await Promise.all(
        req.body.data.map(item => create(user, modelName, item))
      );

      console.log({ createdIds });
      return res.status(200).send(createdIds);
    } catch (e) {
      let code;
      if (e === 'error/model-not-provided') {
        code = 400;
      } else if (e === 'error/permission-denied') {
        code = 403;
      } else {
        code = 500;
      }

      if (e instanceof Error) {
        e = e.message;
      }

      return res.status(code).send({ messages: [ e ] });
    }
  },

  async updateV2(req, res) {
    const user = Auth.user();
    const modelName = req.get("Model");

    try {
      const updatedIds = await Promise.all(
        req.body.data.map(item => update(user, modelName, item))
      );

      console.log({ updatedIds });
      return res.status(200).send(updatedIds);
    } catch (e) {
      let code;
      if (e === 'error/model-not-provided') {
        code = 400;
      } else if (e === 'error/permission-denied') {
        code = 403;
      } else if (e === 'error/record-not-found') {
        code = 400;
      } else {
        code = 500;
      }

      if (e instanceof Error) {
        e = e.message;
      }

      return res.status(code).send({ messages: [ e ] });
    }
  },

  async deleteV2(req, res) {
    const user = Auth.user();
    const modelName = req.get("Model");

    try {
      const deletedIds = await Promise.all(
        req.body.data.map(item => deleteRecord(user, modelName, item))
      );

      console.log({ deletedIds });
      return res.status(200).send(deletedIds);
    } catch (e) {
      let code;
      if (e === 'error/model-not-provided') {
        code = 400;
      } else if (e === 'error/permission-denied') {
        code = 403;
      } else if (e === 'error/record-not-found') {
        code = 400;
      } else {
        code = 500;
      }

      if (e instanceof Error) {
        e = e.message;
      }

      return res.status(code).send({ messages: [ e ] });
    }
  },
};

module.exports = magicController;
