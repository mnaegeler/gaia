const Permission = require("../../facades/Permission");
const { getModelMetadata } = require("../../utils/metadata");

module.exports = {
  logError: function (msg) {
    console.log("\x1b[31m%s\x1b[0m", `(!) ${msg.message}`, msg);
  },

  preActionValidation: async function (modelName, user, action) {
    const models = require("../../models");
    if (!modelName) {
      throw new Error("error/model-not-provided");
    }

    const hasPermission = await Permission.hasPermission(
      modelName,
      action,
      user
    );

    if (!hasPermission) {
      throw new Error("error/permission-denied");
    }

    const modelMeta = await getModelMetadata(modelName);
    return { modelMeta };
  },
};
