const Permission = require("../../facades/Permission");
const { getModelMetadata } = require("../../utils/metadata");
const { makeValidations } = require("../../validation-rules");
const { preActionValidation, logError } = require("./_commonActions");

module.exports = async function create(user, modelName, data) {
  const models = require("../../models");

  const { modelMeta } = await preActionValidation(modelName, user, "create");

  for (let item of data) {
    item.CreatedByUserId = user.id;

    // CreatedByUser Organization
    if (
      !item.OrganizationId &&
      modelMeta.ModelFields.findIndex((f) => f.name === "OrganizationId") > -1
    ) {
      item.OrganizationId = req.get("Organization");
    }

    // User autofill
    if (
      modelMeta.ModelFields.findIndex((f) => f.name === "UserId") > -1 &&
      !item.UserId
    ) {
      item.UserId = user.id;
    }

    // Default Organization
    if (
      modelMeta.ModelFields.findIndex(
        (f) => f.name === "DefaultOrganizationId"
      ) > -1
    ) {
      item.DefaultOrganizationId = req.get("Organization");
    }

    if (
      modelName === "User" &&
      (!item.OrganizationUsers || !item.OrganizationUsers.length)
    ) {
      item.OrganizationUsers = [{ OrganizationId: req.get("Organization") }];
    }

    try {
      await makeValidations(modelMeta, item, user);
    } catch (e) {
      logError(e);
      return res.status(500).send({ messages: [e.message] });
    }

    const bodyToCreate = { ...item };
    const bodyKeys = Object.keys(bodyToCreate);

    for (let bodyIndex = 0; bodyIndex < bodyKeys.length; bodyIndex++) {
      const key = bodyKeys[bodyIndex];
      const metaField = modelMeta.ModelFields.find((m) => m.name === key);
      if (
        metaField &&
        (metaField.type === "integer" || metaField.type === "decimal")
      ) {
        if (bodyToCreate[key] === "") {
          bodyToCreate[key] = null;
        }
      }
    }

    element = await models[modelName].create(bodyToCreate, {
      returning: ["*"],
    });
  }
};
