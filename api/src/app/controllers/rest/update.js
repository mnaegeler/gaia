const { makeValidations } = require("../../validation-rules");
const { preActionValidation, logError } = require('./_commonActions');

module.exports = async function update(user, modelName, data) {
  const models = require("../../models");

  const { modelMeta } = await preActionValidation(modelName, user, "update");

  let element = null;

  try {
    element = await models[modelName].findByPk(data.id);
  } catch (e) {
    throw new Error("error/record-not-found");
  }

  const userId = user.id;

  try {
    await makeValidations(modelMeta, data, user, element.id);
  } catch (e) {
    logError(e);
    throw new Error(e.message);
  }

  const bodyToUpdate = { ...data };

  for (let key in bodyToUpdate) {
    const metaField = modelMeta.ModelFields.find((m) => m.name === key);
    const value = bodyToUpdate[key];

    const isHasMany = Array.isArray(value) === true && metaField.type === "hasMany";
    if (isHasMany) {
      const relationsToRemove = await element[`get${key}`]();
      try {
        bodyToUpdate[key] = bodyToUpdate[key].map(async (arrayItem) => {
          if (arrayItem.id) {
            // If the relation still exists, remove from removal list
            const index = relationsToRemove.findIndex(
              (r) => r.id === arrayItem.id
            );
            if (index !== -1) {
              relationsToRemove.splice(index, 1);
            }
          }

          arrayItem.CreatedByUserId = userId;

          const [ item ] = await models[metaField.targetModel].upsert(
            { ...arrayItem },
            { returning: ["*"] }
          );
          await element[`add${key}`](item);
          return item;
        });
      } catch (e) {
        throw new Error(e);
      }

      for (let rel of relationsToRemove) {
        await rel.destroy();
      }
    }
  }

  try {
    await element.update(data, { returning: "id" });
  } catch (e) {
    logError(e);
    throw new Error(e);
  }

  return element.id;
};
