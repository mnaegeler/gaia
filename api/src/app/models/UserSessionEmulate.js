module.exports = (sequelize, DataTypes) => {
  const UserSessionEmulate = sequelize.define("UserSessionEmulate", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    endDate: DataTypes.DATE,
    startDate: DataTypes.DATE,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  UserSessionEmulate.associate = (models) => {
    UserSessionEmulate.belongsTo(models.Organization, { constraints: false });
    UserSessionEmulate.belongsTo(models.User, {
      as: "TargetUser",
      constraints: false,
    });
    UserSessionEmulate.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return UserSessionEmulate;
};
