module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define("Model", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    color: DataTypes.STRING,
    filters: DataTypes.JSON,
    icon: DataTypes.STRING,
    name: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Model.associate = (models) => {
    Model.hasMany(models.ModelField, { as: "ModelFields", constraints: false });
    Model.hasMany(models.ModelPermission, {
      as: "ModelPermissions",
      constraints: false,
    });
    Model.hasMany(models.ModelValidation, {
      as: "ModelValidations",
      constraints: false,
    });
    Model.hasMany(models.View, { as: "Views", constraints: false });
    Model.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return Model;
};
