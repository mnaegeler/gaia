module.exports = (sequelize, DataTypes) => {
  const View = sequelize.define("View", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    class: DataTypes.STRING,
    filters: DataTypes.JSON,
    include: DataTypes.JSON,
    key: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    showOnMenu: DataTypes.BOOLEAN,
    slug: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  View.associate = (models) => {
    View.belongsTo(models.MenuSection, { constraints: false });
    View.belongsTo(models.Model, { constraints: false });
    View.hasMany(models.ViewAction, { as: "ViewActions", constraints: false });
    View.hasMany(models.ViewField, { as: "ViewFields", constraints: false });
    View.hasMany(models.ViewPermission, {
      as: "ViewPermissions",
      constraints: false,
    });
    View.hasMany(models.ViewRowAction, {
      as: "ViewRowActions",
      constraints: false,
    });
    View.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return View;
};
