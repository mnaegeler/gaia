module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define("Comment", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    comment: DataTypes.TEXT,
    recordId: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Comment.associate = (models) => {
    Comment.belongsTo(models.Model, { constraints: false });
    Comment.belongsTo(models.Organization, { constraints: false });
    Comment.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return Comment;
};
