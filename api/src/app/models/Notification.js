module.exports = (sequelize, DataTypes) => {
  const Notification = sequelize.define("Notification", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    content: DataTypes.TEXT,
    link: DataTypes.STRING,
    originRecordId: DataTypes.STRING,
    read: DataTypes.BOOLEAN,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Notification.associate = (models) => {
    Notification.belongsTo(models.Model, {
      as: "OriginModel",
      constraints: false,
    });
    Notification.belongsTo(models.Organization, { constraints: false });
    Notification.belongsTo(models.User, {
      as: "TargetUser",
      constraints: false,
    });
    Notification.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return Notification;
};
