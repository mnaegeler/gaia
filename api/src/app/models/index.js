"use strict";

const fs = require("fs");
const path = require("path");
const { Sequelize, DataTypes, QueryTypes } = require("sequelize");
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "development";
const config = require("../../config/config")[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else if (env === "test") {
  sequelize = new Sequelize("sqlite::memory:", config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(sequelize, DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

sequelize.addHook("afterUpdate", async (instance, options) => {
  const modelName = instance.constructor.name;
  // prevent recursion
  if (modelName !== "Log") {
    const data = {
      values: instance.dataValues,
      previousValues: instance._previousDataValues,
    };

    await sequelize.query(
      `INSERT INTO "Logs" ("function", "method", "modelName", "path", "query", "statusCode", "timing") VALUES (
      'globalAfterUpdate',
      NULL,
      :modelName,
      :path,
      :query,
      NULL,
      NULL
    )`,
      {
        replacements: {
          modelName,
          path: data.values.id,
          query: JSON.stringify(data),
        },
        type: QueryTypes.INSERT,
      }
    );
  }
});

module.exports = db;
