module.exports = (sequelize, DataTypes) => {
  const ModelPermission = sequelize.define("ModelPermission", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    action: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelPermission.associate = (models) => {
    ModelPermission.belongsTo(models.Model, { constraints: false });
    ModelPermission.belongsTo(models.Role, { constraints: false });
    ModelPermission.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return ModelPermission;
};
