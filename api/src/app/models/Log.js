module.exports = (sequelize, DataTypes) => {
  const Log = sequelize.define("Log", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    function: DataTypes.STRING,
    method: DataTypes.STRING,
    modelName: DataTypes.STRING,
    path: DataTypes.STRING,
    query: DataTypes.JSON,
    statusCode: DataTypes.INTEGER,
    timing: DataTypes.DECIMAL(12, 2),
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  Log.associate = (models) => {
    Log.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return Log;
};
