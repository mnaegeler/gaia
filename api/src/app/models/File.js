module.exports = (sequelize, DataTypes) => {
  const File = sequelize.define("File", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    label: DataTypes.STRING,
    link: DataTypes.TEXT,
    mimetype: DataTypes.STRING,
    size: DataTypes.INTEGER,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  File.associate = (models) => {
    File.belongsTo(models.Organization, { constraints: false });
    File.belongsTo(models.User, { as: "CreatedByUser", constraints: false });
  };

  return File;
};
