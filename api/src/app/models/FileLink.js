module.exports = (sequelize, DataTypes) => {
  const FileLink = sequelize.define("FileLink", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    recordId: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  FileLink.associate = (models) => {
    FileLink.belongsTo(models.File, { constraints: false });
    FileLink.belongsTo(models.Model, { constraints: false });
    FileLink.belongsTo(models.Organization, { constraints: false });
    FileLink.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return FileLink;
};
