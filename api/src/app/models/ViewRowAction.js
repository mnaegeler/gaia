module.exports = (sequelize, DataTypes) => {
  const ViewRowAction = sequelize.define("ViewRowAction", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },
    class: DataTypes.STRING,
    labelKey: DataTypes.STRING,
    order: DataTypes.STRING,
    params: DataTypes.JSON,
    position: DataTypes.STRING,
    slug: DataTypes.STRING,
    type: DataTypes.STRING,
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ViewRowAction.associate = (models) => {
    ViewRowAction.belongsTo(models.View, { constraints: false });
    ViewRowAction.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return ViewRowAction;
};
