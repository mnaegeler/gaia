module.exports = (sequelize, DataTypes) => {
  const ModelFieldPermission = sequelize.define("ModelFieldPermission", {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: sequelize.literal("uuid_generate_v4()"),
    },

    createdAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
      allowNull: false,
    },
  });

  ModelFieldPermission.associate = (models) => {
    ModelFieldPermission.belongsTo(models.Model, { constraints: false });
    ModelFieldPermission.belongsTo(models.ModelField, { constraints: false });
    ModelFieldPermission.belongsTo(models.Organization, { constraints: false });
    ModelFieldPermission.belongsTo(models.Role, { constraints: false });
    ModelFieldPermission.belongsTo(models.User, {
      as: "CreatedByUser",
      constraints: false,
    });
  };

  return ModelFieldPermission;
};
