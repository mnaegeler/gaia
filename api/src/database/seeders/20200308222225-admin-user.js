'use strict';

module.exports = {
  up: async (queryInterface) => {
    const [,ptLang] = await queryInterface.bulkInsert('Languages', [
      {
        id: '8158e21c-6b98-413e-a1db-3610cd69d52c',
        abbr: 'en',
        name: 'English',
        active: false,
      },
      {
        id: '69c6921e-a832-4370-aba1-642045c534d3',
        abbr: 'pt_BR',
        name: 'Português',
        active: true,
      },
    ], { returning: ['id'] })

    const models = require('../../app/models');
    
    const sysAdminRole = await models["Role"].create(
      {
        hasFullAccess: true,
        id: "be45ccd8-3d92-4f59-bc86-79d4026625a8",
        labelKey: "SYSTEM_ADMINISTRATOR",
        level: 1,
        reference: "SYSTEM_ADMINISTRATOR",
      },
      { returning: ["id"] }
    );
    // const sysAdminRole = await models['Role'].findOne({ where: { reference: 'SYSTEM_ADMINISTRATOR' } });

    const defaultUserRole = await models["Role"].create(
      {
        hasFullAccess: false,
        id: "37b79ec8-56d2-4e67-892a-e6ad3b79a21a",
        labelKey: "DEFAULT_USER",
        level: 2,
        reference: "DEFAULT_USER",
      },
      { returning: ["id"] }
    );
    // const defaultUserRole = await models['Role'].findOne({ where: { reference: 'DEFAULT_USER' } });

    const [defaultOrg] = await queryInterface.bulkInsert('Organizations', [
      {
        id: '82513cbc-7bdd-4340-8563-810d1bb96ee7',
        name: 'Default Org',
      },
    ], { returning: ['id'] })

    const [adminUser, defaultUser] = await queryInterface.bulkInsert('Users', [
      {
        id: '2cd40d9b-e4b9-41b2-9dc9-c2e9052f4cfc',
        name: 'Admin',
        email: 'admin@supernova.test',
        // password: admin123
        passwordHash: '$2a$08$iMgk32kr.Zzf7Y0SYgWXf.9mEcIbO.vsrJkEg.Vp.OzvL6XsMioVa',
        LanguageId: ptLang.id,
        RoleId: sysAdminRole.id,
        DefaultOrganizationId: defaultOrg.id,
      },

      {
        id: 'f9d1d052-a0a6-436d-84d1-7215633a6f2d',
        name: 'User',
        email: 'user@supernova.test',
        // password: user123
        passwordHash: '$2a$08$sMZzDHrqzWI9FMAVuFlrq.QiGGhPXXKN5euTnbSvhWEkC6pw6pGQm',
        LanguageId: ptLang.id,
        RoleId: defaultUserRole.id,
        DefaultOrganizationId: defaultOrg.id,
      },
    ], { returning: ['id'] });
    
    await queryInterface.bulkInsert('OrganizationUsers', [
      {
        OrganizationId: defaultOrg.id,
        UserId: adminUser.id,
      },
      {
        OrganizationId: defaultOrg.id,
        UserId: defaultUser.id,
      },
    ])
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('Users', null, { truncate: true });
  }
};
