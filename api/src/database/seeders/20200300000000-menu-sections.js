'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('MenuSections', [
      {
        id: '2f9020dc-ee9f-446e-931a-b6217ee41c8b',
        labelKey: 'PrimaryMenuSection'
      },
      {
        id: '8dcd077b-b723-4b33-8c80-48702c9afdf4',
        labelKey: 'SetupMenuSection'
      },
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MenuSections', null, { truncate: true });
  }
};
