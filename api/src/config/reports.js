const config = {
  // 1: {
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //     // columns: ['Reading.taskCode', 'Import.batchCode'],
  //   },
  //   data: {
  //     columns: [
  //       `"Reading"."id"`,
  //     ],
  //   },
  //   viewType: "single-number",
  //   label: "Leituras para o mês",
  // },
  
  // 2: {
  //   title: "Report 2",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   filters: `"Reading"."readingDate" is not null`,
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //   },
  //   viewType: "single-number",
  //   label: "Leituras realizadas no mês",
  // },

  // 3: {
  //   title: "Report 3",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   filters: `"Reading"."readingDate" is null`,
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //   },
  //   viewType: "single-number",
  //   label: "Leituras pendentes",
  // },

  // 4: {
  //   title: "Report 4",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   filters: `"Reading"."readingDate" is null AND "Import"."expectedEndDate" < NOW()`,
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //   },
  //   viewType: "single-number",
  //   label: "Leituras atrasadas",
  // },
  // // 5: {
  // //   title: "Report 5",
  // //   model: "Reading",
  // //   include: ["Import"],
  // //   intervalFilterField: "Import.expectedStartDate",
  // //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  // //   filters: [
  // //     '"Reading"."readingDate" is not null',
  // //   ],
  // //   summary: {
  // //       grouper: 'Import.expectedStartDate',
  // //       summarizedAttribute: 'Reading.id',
  // //       summarizedFunction: 'count', // count,sum,avg,min,max
  // //   },
  // //   viewType: 'single-number',
  // //   label: 'Repasses',
  // // },

  // 6: {
  //   title: "Report 6",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "1 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   filters: `"Reading"."occurrenceCode" is not null AND "Reading"."occurrenceCode" != ''`,
  //   summary: {
  //     grouper: `"Reading"."occurrenceCode"`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //   },
  //   viewType: "chart",
  //   label: "Ocorrências no mês",
  // },

  // 7: {
  //   title: "Report 7",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "6 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   // filters: `"Reading"."occurrenceCode" is not null AND "Reading"."occurrenceCode" != ''`,
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     summarizedAttribute: `COUNT("Reading"."id")`, // count,sum,avg,min,max
  //   },
  //   viewType: "chart",
  //   label: "Crescimento vegetativo",
  // },

  // 8: {
  //   title: "Report 8",
  //   model: "Reading",
  //   include: ["Import"],
  //   intervalFilterField: "Import.expectedStartDate",
  //   intervalFilterValue: "6 MONTHS", // {number} MONTHS, {number} DAYS, THIS_MONTH
  //   filters: `"Reading"."occurrenceCode" is not null AND "Reading"."occurrenceCode" != ''`,
  //   summary: {
  //     grouper: `DATE_TRUNC('month', "Import"."expectedStartDate")`,
  //     // grouper: `"Reading"."occurrenceCode"`,
  //     summarizedAttribute: `COUNT("Reading"."occurrenceCode")`, // count,sum,avg,min,max
  //   },
  //   viewType: "chart",
  //   label: "Benchmark de ocorrências",
  // },
};

module.exports = config;
