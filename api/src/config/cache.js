const NodeCache = require( "node-cache" );

module.exports = (function () {
  const cache = new NodeCache();

  cache.addListener('clearMetadata', () => {
    const keysToDelete = cache.keys().filter(key => key.startsWith('metadata_'));
    cache.del(keysToDelete);
  })
  
  return cache;
})();
