require('dotenv').config()
// const hooks = require('../app/triggers');

module.exports = {
  "development": {
    // use_env_variable: 'DEV_DB_CONN',
    "username": process.env.DEV_DB_USER,
    "password": process.env.DEV_DB_PASSWORD,
    "database": process.env.DEV_DB_NAME,
    "host": process.env.DEV_DB_HOST,
    "dialect": "postgres",
    // "dialectOptions": {
    //   "ssl": "disable"
    // },
    "logging": (str) => {
      // console.log("SQL QUERY:");
      // console.log(str);
      // console.log("==========");
    },
    "define": {
      "paranoid": true,
      "timestamp": true,
      "hooks": require('../app/triggers')
    }
  },
  "test": {
    "username": process.env.TEST_DB_USER,
    "password": process.env.TEST_DB_PASSWORD,
    "database": process.env.TEST_DB_NAME,
    "host": process.env.TEST_DB_HOST,
    "dialect": "postgres",
    "logging": false,
    "define": {
      "paranoid": true,
      "timestamp": true,
      "hooks": require('../app/triggers')
    }
  },
  "production": {
    "username": process.env.PROD_DB_USER,
    "password": process.env.PROD_DB_PASSWORD,
    "database": process.env.PROD_DB_NAME,
    "host": process.env.PROD_DB_HOST,
    "dialect": "postgres",
    "logging": false,
    "define": {
      "paranoid": true,
      "timestamp": true,
      "hooks": require('../app/triggers')
    }
  }
}
