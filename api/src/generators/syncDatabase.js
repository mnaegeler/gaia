const models = require("../app/models");

async function main() {
  let modelName = process.env.MODEL_NAME;
//   modelName = "View";

  console.time("sync models");
  if (modelName) {
    await models[modelName].sync({ alter: true });
  } else {
    await models.sequelize.sync({ alter: true });
  }

  console.timeEnd("sync models");
  process.exit(0);
}

main();
