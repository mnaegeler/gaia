const fs = require("fs").promises;
const path = require("path");
// const prettier = require("prettier")

// Paths
const metadataPath = path.resolve(__dirname, "..", "app", "metadata");
const permissionsMetadataPath = path.resolve(
  __dirname,
  "..",
  "app",
  "permissions-metadata"
);

let models;
// let models = require("../app/models");

const IGNORE_FIELDS = [
  "createdAt",
  "updatedAt",
  // "deletedAt",
  "CreatedByUserId",
  "OrganizationId",
];

async function makeDirs(folder) {
  const folders = [
    "",
    "ModelFields",
    "ModelValidations",
    "Views",
    "ViewFields",
    "ViewActions",
    "ViewRowActions",
  ];

  // await Promise.all(
  //   folders.map(async (folderName) => {
  //     try {
  //       console.log('criando path: ', metadataPath, folder, folderName);
  //       await fs.mkdir(path.resolve(metadataPath, folder, folderName));
  //     } catch (e) {}
  //   })
  // );

  try {
    await fs.mkdir(path.resolve(metadataPath, folder));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "ModelFields"));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "ModelValidations"));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "Views"));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "ViewFields"));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "ViewActions"));
  } catch (e) {
    console.error(e);
  }

  try {
    await fs.mkdir(path.resolve(metadataPath, folder, "ViewRowActions"));
  } catch (e) {
    console.error(e);
  }
}

async function writeFiles(modelName, fieldName, data) {
  if (!data) {
    return;
  }
  
  await Promise.all(
    data.map(async (field) => {
      const filePath = path.resolve(
        metadataPath,
        modelName,
        fieldName,
        `${field.id}.json`
      );

      const properties = Object.keys(field)
        .filter((f) => !IGNORE_FIELDS.includes(f))
        .sort((a, b) => {
          if (a < b) {
            return -1;
          }

          if (a > b) {
            return 1;
          }

          return 0;
        });
      const fileData = {};
      properties.forEach((prop) => {
        fileData[prop] = field[prop];
      });

      const fileBuffer = JSON.stringify(fileData, null, 2);
      await fs.writeFile(filePath, fileBuffer);
    })
  );
}

async function writeMetadataModel(model) {
  const filePath = path.resolve(metadataPath, model.name, `Model.json`);
  const properties = Object.keys(JSON.parse(JSON.stringify(model)))
    .filter((f) => !["ModelFields", "ModelValidations", "Views"].includes(f))
    .filter((f) => !IGNORE_FIELDS.includes(f))
    .sort((a, b) => {
      if (a < b) {
        return -1;
      }

      if (a > b) {
        return 1;
      }

      return 0;
    });

  const modelData = {};
  properties.forEach((f) => {
    modelData[f] = model[f];
  });

  const fileBuffer = JSON.stringify(modelData, null, 2);
  // const pretty = prettier.format(fileBuffer, { parser: 'json' })
  await fs.writeFile(filePath, fileBuffer);
}

async function writeMetadataViews(model) {
  await model.Views.forEach(async (view) => {
    const filePath = path.resolve(
      metadataPath,
      model.name,
      `Views`,
      `${view.id}.json`
    );

    const properties = Object.keys(view)
      .filter((f) => !IGNORE_FIELDS.includes(f))
      .filter(
        (prop) =>
          !["ViewActions", "ViewRowActions", "ViewFields"].includes(prop)
      )
      .sort((a, b) => {
        if (a < b) {
          return -1;
        }

        if (a > b) {
          return 1;
        }

        return 0;
      });

    const fileData = {};
    properties.forEach((prop) => {
      fileData[prop] = view[prop];
    });

    const fileBuffer = JSON.stringify(fileData, null, 2);
    await Promise.all([
      await fs.writeFile(filePath, fileBuffer),
      await writeFiles(model.name, 'ViewFields', view['ViewFields']),
      await writeFiles(model.name, 'ViewActions', view['ViewActions']),
      await writeFiles(model.name, 'ViewRowActions', view['ViewRowActions']),
    ]);
  });
}

async function writeMetadataRoles(role) {
  try {
    await fs.mkdir(path.join(permissionsMetadataPath, role.reference));
  } catch (e) {}
  try {
    await fs.mkdir(
      path.join(permissionsMetadataPath, role.reference, "ModelPermissions")
    );
  } catch (e) {}
  try {
    await fs.mkdir(
      path.join(permissionsMetadataPath, role.reference, "ViewPermissions")
    );
  } catch (e) {}
  try {
    await fs.mkdir(
      path.join(
        permissionsMetadataPath,
        role.reference,
        "ModelFieldPermissions"
      )
    );
  } catch (e) {}

  const filePath = path.resolve(
    permissionsMetadataPath,
    role.reference,
    `Role.json`
  );

  const fields = [
    "deletedAt",
    "hasFullAccess",
    "id",
    "labelKey",
    "level",
    "reference",
  ];

  const properties = Object.keys(role.dataValues)
    .filter((f) => fields.includes(f))
    .sort((a, b) => {
      if (a < b) {
        return -1;
      }

      if (a > b) {
        return 1;
      }

      return 0;
    });

  const fileData = {};
  for (const prop of properties) {
    fileData[prop] = role[prop];
  }

  const fileBuffer = JSON.stringify(fileData, null, 2);
  await fs.writeFile(filePath, fileBuffer);

  for (const modelPerm of role.ModelPermissions) {
    const modelPermissionsFilePath = path.resolve(
      permissionsMetadataPath,
      role.reference,
      `ModelPermissions`,
      `${modelPerm.id}.json`
    );

    const modelPermissionsFileData = {};
    const modelPermKeys = Object.keys(modelPerm.dataValues).filter(
      (f) =>
        ![
          "createdAt",
          "updatedAt",
          "CreatedByUserId",
          "OrganizationId",
        ].includes(f)
    );
    for (const key in modelPermKeys) {
      let k = modelPermKeys[key];
      modelPermissionsFileData[k] = modelPerm[k];
    }

    const modelPermissionsFileBuffer = JSON.stringify(
      modelPermissionsFileData,
      null,
      2
    );
    await fs.writeFile(modelPermissionsFilePath, modelPermissionsFileBuffer);
  }

  for (const modelPerm of role.ViewPermissions) {
    const modelPermissionsFilePath = path.resolve(
      permissionsMetadataPath,
      role.reference,
      `ViewPermissions`,
      `${modelPerm.id}.json`
    );

    const modelPermissionsFileData = {};
    const modelPermKeys = Object.keys(modelPerm.dataValues).filter(
      (f) =>
        ![
          "createdAt",
          "updatedAt",
          "CreatedByUserId",
          "OrganizationId",
        ].includes(f)
    );
    for (const key in modelPermKeys) {
      let k = modelPermKeys[key];
      modelPermissionsFileData[k] = modelPerm[k];
    }

    const modelPermissionsFileBuffer = JSON.stringify(
      modelPermissionsFileData,
      null,
      2
    );
    await fs.writeFile(modelPermissionsFilePath, modelPermissionsFileBuffer);
  }

  for (const modelPerm of role.ModelFieldPermissions) {
    const modelPermissionsFilePath = path.resolve(
      permissionsMetadataPath,
      role.reference,
      `ModelFieldPermissions`,
      `${modelPerm.id}.json`
    );

    const modelPermissionsFileData = {};
    const modelPermKeys = Object.keys(modelPerm.dataValues).filter(
      (f) =>
        ![
          "createdAt",
          "updatedAt",
          "CreatedByUserId",
          "OrganizationId",
        ].includes(f)
    );
    for (const key in modelPermKeys) {
      let k = modelPermKeys[key];
      modelPermissionsFileData[k] = modelPerm[k];
    }

    const modelPermissionsFileBuffer = JSON.stringify(
      modelPermissionsFileData,
      null,
      2
    );
    await fs.writeFile(modelPermissionsFilePath, modelPermissionsFileBuffer);
  }
}

async function exportModelMetadata(modelName) {
  console.time("exportModelMetadata");
  try {
    await fs.rm(path.resolve(metadataPath, modelName), { recursive: true });
  } catch (e) {
    console.log(e);
  }

  if (models === undefined || models["Model"] === undefined) {
    models = require("../app/models");
  }

  const model = await models["Model"].findOne({
    where: { name: modelName },
    include: [
      {
        model: models["ModelField"],
        as: "ModelFields",
        orderBy: [["createdAt", "ASC"]],
      },
      {
        model: models["ModelValidation"],
        as: "ModelValidations",
        orderBy: [["createdAt", "ASC"]],
      },
      {
        model: models["View"],
        as: "Views",
        orderBy: [["createdAt", "ASC"]],
        include: [
          {
            model: models["ViewField"],
            as: "ViewFields",
            orderBy: [["createdAt", "ASC"]],
          },
          {
            model: models["ViewAction"],
            as: "ViewActions",
            orderBy: [["createdAt", "ASC"]],
          },
          {
            model: models["ViewRowAction"],
            as: "ViewRowActions",
            orderBy: [["createdAt", "ASC"]],
          },
        ],
      },
    ],
  });

  try {
    let m = JSON.parse(JSON.stringify(model));
    await makeDirs(m.name);

    await writeMetadataModel(m);
    await writeFiles(m.name, 'ModelFields', m['ModelFields']);
    await writeFiles(m.name, 'ModelValidations', m['ModelValidations']);
    await writeMetadataViews(m);
    console.timeEnd("exportModelMetadata");
  } catch (e) {
    console.error(e);
  }
}

async function exportAllMetadata() {
  console.time("exportAllMetadata");
  try {
    await fs.rm(metadataPath, { recursive: true });
  } catch (e) {
    console.log(e);
  }

  // try {
  //   await fs.rm(permissionsMetadataPath, { recursive: true });
  // } catch (e) {
  //   console.log(e);
  // }

  try {
    await fs.mkdir(metadataPath);
  } catch (e) {}

  // try {
  //   await fs.mkdir(permissionsMetadataPath);
  // } catch (e) {}

  if (models === undefined || models["Model"] === undefined) {
    models = require("../app/models");
  }

  const meta = await models["Model"].findAll({
    paranoid: false,
    include: [
      {
        model: models["ModelField"],
        as: "ModelFields",
        orderBy: [["createdAt", "ASC"]],
        paranoid: false,
      },
      {
        model: models["ModelValidation"],
        as: "ModelValidations",
        orderBy: [["createdAt", "ASC"]],
        paranoid: false,
      },
      {
        model: models["View"],
        as: "Views",
        orderBy: [["createdAt", "ASC"]],
        paranoid: false,
        include: [
          {
            model: models["ViewField"],
            as: "ViewFields",
            orderBy: [["createdAt", "ASC"]],
            paranoid: false,
          },
          {
            model: models["ViewAction"],
            as: "ViewActions",
            orderBy: [["createdAt", "ASC"]],
            paranoid: false,
          },
          {
            model: models["ViewRowAction"],
            as: "ViewRowActions",
            orderBy: [["createdAt", "ASC"]],
            paranoid: false,
          },
        ],
      },
    ],
  });

  // const roles = await models["Role"].findAll({
  //   include: ["ModelPermissions", "ViewPermissions", "ModelFieldPermissions"],
  //   paranoid: false,
  // });

  try {
    for (let index = 0; index < meta.length; index++) {
      const model = meta[index];
      let m = JSON.parse(JSON.stringify(model));
      await makeDirs(m.name);
      await writeMetadataModel(m);
      await writeFiles(m.name, 'ModelFields', m['ModelFields']);
      await writeFiles(m.name, 'ModelValidations', m['ModelValidations']);
      // await writeMetadataModelFields(m);
      // await writeMetadataModelValidations(m);
      await writeMetadataViews(m);
    }

    // for (let index = 0; index < roles.length; index++) {
    //   const role = roles[index];
    //   await writeMetadataRoles(role);
    // }

    console.timeEnd("exportAllMetadata");
    process.exit(0);
  } catch (e) {
    console.error(e);
  }
}

process.argv.forEach((val, index) => {
  if (
    val === "-s" &&
    process.argv[index - 1].includes("src/generators/export.js")
  ) {
    exportAllMetadata();
  }
});

module.exports = {
  exportAllMetadata,
  exportModelMetadata,
};
