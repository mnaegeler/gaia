const util = require("util");
const path = require("path");
const { DataTypes } = require("sequelize");

async function synchronizeDatabase(modelName = null) {
  console.time("synchronizeDatabase");
  const models = require("../app/models");

  if (!modelName) {
    await models.sequelize.sync({ alter: true });
    console.timeEnd("synchronizeDatabase");
    return;
  }

  const modelsPath = path.resolve(__dirname, "..", "app", "models");
  const filePath = path.resolve(modelsPath, `${modelName}.js`);

  delete require.cache[require.resolve(filePath)];
  const updatedModel = require(filePath)(models.sequelize, DataTypes);

  models[modelName] = updatedModel;
  models[modelName].associate(models);

  await models[modelName].sync({ alter: { drop: false } });
  console.timeEnd("synchronizeDatabase");
}

process.argv.forEach((val, index) => {
  if (
    val === "-s" &&
    process.argv[index - 1].includes("src/generators/sync-database.js")
  ) {
    synchronizeDatabase();
  }
});

module.exports = {
  synchronizeDatabase,
};
