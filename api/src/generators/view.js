const models = require("../app/models");
const {
  Model,
  View,
  ViewAction,
  ViewField,
  MenuSection,
} = models;
const AVAILABLE_TYPES = ["ShowView", "FormView", "ListView"];

let willEval = false;
let model;
let type;

function camelToSnake(str) {
  return str.replace(
    /[A-Z]/g,
    (letter, index) => `${index > 0 ? "_" : ""}${letter.toLowerCase()}`
  );
}

async function generateView() {
  const primaryMenu = await MenuSection.findOne({
    where: { labelKey: "PrimaryMenuSection" },
  });

  const modelInstance = await Model.findOne({
    where: {
      name: model,
    },
    include: ["ModelFields", "ModelValidations"],
  });

  const slugName = camelToSnake(modelInstance.name);

  const viewData = {
    ModelId: modelInstance.id,
    labelKey: modelInstance.name,
    type: type,
    showOnMenu: false,
    filters: null,
    include: null,
  };

  if (type === "ShowView") {
    viewData.key = `show-${slugName}`;
    viewData.slug = `/${slugName}_view`;
  } else if (type === "ListView") {
    viewData.key = `list-${slugName}`;
    viewData.slug = `/${slugName}`;
    viewData.MenuSectionId = primaryMenu.id;
  } else if (type === "FormView") {
    viewData.key = `form-${slugName}`;
    viewData.slug = `/${slugName}_form`;
  }

  const view = await View.create(viewData);

  if (type === "ShowView") {
    await ViewAction.create({
      ViewId: view.id,
      labelKey: "edit",
      class: null,
      type: "navigation",
      slug: `/${slugName}_form/:id:`,
      order: 1,
      position: "top right",
    });

    const modelFields = modelInstance.ModelFields.filter(
      (f) =>
        !["createdAt", "updatedAt", "deletedAt", "CreatedByUserId"].includes(f.name)
    );

    const fields = [];
    modelFields.forEach((field, index) => {
      fields.push({
        ModelFieldId: field.id,
        order: index,
        class: "is-2",
        labelKey: field.name,
      });
    });

    const createdAt = modelInstance.ModelFields.find(
      (f) => f.name === "createdAt"
    );
    const updatedAt = modelInstance.ModelFields.find(
      (f) => f.name === "updatedAt"
    );
    const ownerId = modelInstance.ModelFields.find((f) => f.name === "CreatedByUserId");

    const dateFields = []
    if (createdAt) {
      dateFields.push({
        ModelFieldId: createdAt.id,
        order: modelFields.length,
        class: "is-2",
        canUpdate: false,
        labelKey: "createdAt",
      });
    }
    if (updatedAt) {
      dateFields.push({
        ModelFieldId: updatedAt.id,
        order: modelFields.length + 1,
        class: "is-12",
        canUpdate: false,
        labelKey: "updatedAt",
      });
    }

    const ownerFields = []
    if (ownerId) {
      ownerFields.push({
        ModelFieldId: ownerId.id,
        order: modelFields.length + 2,
        class: "is-2",
        canUpdate: false,
        labelKey: "CreatedByUserId",
      });
    }

    const viewFields = [...fields, ...dateFields, ...ownerFields].map((f) => {
      let nf = { ...f };
      nf.ViewId = view.id;
      return nf;
    });

    await ViewField.bulkCreate([...viewFields]);
//   } else if (type === "ListView") {
//   } else if (type === "FormView") {
  }
}

process.argv.forEach((val, index) => {
  if (val === "-s" && process.argv[index - 1].includes('src/generators/view.js')) {
    willEval = true;
  }

  if (models[val]) {
    model = val;
  }

  let hasType = AVAILABLE_TYPES.indexOf(val);
  if (hasType > -1) {
    type = AVAILABLE_TYPES[hasType];
  }

  if (willEval && model && type) {
    generateView();
  }
});

module.exports = {
  generateView,
};
