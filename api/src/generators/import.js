const fs = require("fs").promises;
// const util = require('util')
const path = require("path");
const {
  Model,
  ModelField,
  View,
  ViewField,
  ViewAction,
  ViewRowAction,
  ModelValidation,
  Role,
  ModelPermission,
  ViewPermission,
  ModelFieldPermission,
} = require("../app/models");

const metadataPath = path.resolve(__dirname, "..", "app", "metadata");
const permissionsMetadataPath = path.resolve(
  __dirname,
  "..",
  "app",
  "permissions-metadata"
);

// If you want to filter out models
const modelsNotToUpdate = [];

async function readAndParseFile(path) {
  const file = await fs.readFile(path, "utf-8");
  try {
    return JSON.parse(file);
  } catch (e) {
    console.log("Error on parse:", path);
    throw new Error(e);
  }
}

async function init(forceExit = true) {
  console.time("importAllMetadata");
  const metaModels = await fs.readdir(metadataPath);
  const modelsToUpdate = metaModels.filter(
    (modelName) => modelsNotToUpdate.includes(modelName) === false
  );

  // This import step needs to be done in two steps because of Foreign Keys on ChildViewId

  // Models, Models Fields and Views
  try {
    await Promise.all(
      modelsToUpdate.map(async (modelName) => {
        // console.log('reading', modelName);
        let model;
        try {
          model = await readAndParseFile(
            path.resolve(metadataPath, modelName, "Model.json")
          );
        } catch(e) {
          return;
        }

        // Model
        await Model.upsert(model, { returning: ["id"] });

        let metaModelFields = [];
        let metaModelValidations = [];
        let metaViews = [];

        try {
          metaModelFields = await fs.readdir(
            path.resolve(metadataPath, modelName, "ModelFields")
          );
        } catch (e) {}

        try {
          metaModelValidations = await fs.readdir(
            path.resolve(metadataPath, modelName, "ModelValidations")
          );
        } catch (e) {}

        try {
          metaViews = await fs.readdir(
            path.resolve(metadataPath, modelName, "Views")
          );
        } catch (e) {}

        await Promise.all([
          ...metaModelFields.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "ModelFields", file)
            );
            await ModelField.upsert(data, { returning: ["id"] });
          }),

          ...metaModelValidations.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "ModelValidations", file)
            );
            await ModelValidation.upsert(data, { returning: ["id"] });
          }),

          ...metaViews.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "Views", file)
            );
            await View.upsert(data, { returning: ["id"] });
          }),
        ]);
      })
    );
  } catch (e) {
    console.error(e);
    throw new Error(e);
  }

  // View Fields, Actions and Row Actions
  try {
    await Promise.all(
      modelsToUpdate.map(async (modelName) => {
        let metaViewFields = [];
        let metaViewActions = [];
        let metaViewRowActions = [];

        try {
          metaViewFields = await fs.readdir(
            path.resolve(metadataPath, modelName, "ViewFields")
          );
        } catch (e) {}

        try {
          metaViewActions = await fs.readdir(
            path.resolve(metadataPath, modelName, "ViewActions")
          );
        } catch (e) {}

        try {
          metaViewRowActions = await fs.readdir(
            path.resolve(metadataPath, modelName, "ViewRowActions")
          );
        } catch (e) {}

        await Promise.all([
          ...metaViewFields.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "ViewFields", file)
            );
            await ViewField.upsert(data, { returning: ["id"] });
          }),

          ...metaViewActions.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "ViewActions", file)
            );
            await ViewAction.upsert(data, { returning: ["id"] });
          }),

          ...metaViewRowActions.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(metadataPath, modelName, "ViewRowActions", file)
            );
            await ViewRowAction.upsert(data, { returning: ["id"] });
          }),
        ]);
      })
    );

    console.timeEnd("importAllMetadata");

    /*
    console.time("importRolesAndPermissions");
    const roles = await fs.readdir(permissionsMetadataPath);
    await Promise.all(
      roles.map(async (role) => {
        const roleData = await readAndParseFile(
          path.resolve(permissionsMetadataPath, role, "Role.json")
        );
        await Role.upsert(roleData, { returning: ["id"] });
        
        let modelPermissions, viewPermissions, modelFieldPermissions;

        try {
          modelPermissions = await fs.readdir(
            path.resolve(permissionsMetadataPath, role, "ModelPermissions")
          );
        } catch (e) {
          modelPermissions = [];
        }
        try {
          viewPermissions = await fs.readdir(
            path.resolve(permissionsMetadataPath, role, "ViewPermissions")
          );
        } catch (e) {
          viewPermissions = [];
        }
        try {
          modelFieldPermissions = await fs.readdir(
            path.resolve(permissionsMetadataPath, role, "ModelFieldPermissions")
          );
        } catch (e) {
          modelFieldPermissions = [];
        }

        await Promise.all([
          ...modelPermissions.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(
                permissionsMetadataPath,
                role,
                "ModelPermissions",
                file
              )
            );
            await ModelPermission.upsert(data, { returning: ["id"] });
          }),

          ...viewPermissions.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(
                permissionsMetadataPath,
                role,
                "ViewPermissions",
                file
              )
            );
            await ViewPermission.upsert(data, { returning: ["id"] });
          }),

          ...modelFieldPermissions.map(async (file) => {
            const data = await readAndParseFile(
              path.resolve(
                permissionsMetadataPath,
                role,
                "ModelFieldPermissions",
                file
              )
            );
            await ModelFieldPermission.upsert(data, { returning: ["id"] });
          }),
        ]);
      })
    );
    console.timeEnd("importRolesAndPermissions");
    */
   
    if (forceExit) {
      process.exit(0);
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  }
}

process.argv.forEach((val, index) => {
  if (
    val === "-s" &&
    process.argv[index - 1].includes("src/generators/import.js")
  ) {
    init();
  }
});

module.exports = {
  init,
};
