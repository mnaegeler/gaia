const { performance } = require("perf_hooks");
const { Log, File } = require("./app/models");
const Auth = require("./app/facades/Auth");
const routes = require("express").Router();
const axios = require("axios");
const multer = require("multer");
const { Storage } = require("@google-cloud/storage");
const fs = require("fs").promises;

const authMiddleware = require("./app/middleware/auth");
const sessionController = require("./app/controllers/sessionController");
const metadataController = require("./app/controllers/metadataController");
const magicController = require("./app/controllers/magicController");
const path = require("path");

const upload = multer({ dest: path.resolve("/tmp") });
let storage, diskStoragePath;
if (process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  storage = new Storage();
} else {
  let p = process.env.FILE_STORAGE_PATH || "/public/uploads";
  if (p.startsWith("/")) {
    p = p.slice(1, p.length);
  }

  diskStoragePath = path.resolve(__dirname, "..", p);
}

async function logEndpoint({ req, res, next }, endpoint) {
  const perf = performance.now();
  let endpointResult;
  try {
    endpointResult = await endpoint(req, res, next);
  } catch (error) {
    console.error("--- ERROR ---");
    console.error(error);
    console.error("--- ----- ---");

    endpointResult = res
      .status(500)
      .send({ message: error.message, name: error.name, stack: error.stack });
  }

  const loggedMethods = ['POST', 'PUT', 'PATCH', 'DELETE'];
  const restrictedPaths = ['/api/sessions'];
  
  if (loggedMethods.includes(req.method) && !restrictedPaths.includes(req.path) && endpoint.name && endpoint.name !== "authMiddleware") {
    let data = req.query;
    if (['POST', 'PUT', 'PATCH'].includes(req.method)) {
      data = req.body;
    }
    
    const dataLog = {
      CreatedByUserId: Auth.user() ? Auth.user().id : null,
      function: endpoint.name,
      method: req.method,
      path: req.path,
      query: data,
      statusCode: res.statusCode,
      modelName: req.get("Model"),
      timing: performance.now() - perf,
    };

    // Later on, add request body and responses for more descriptive logs (express-mung).
    Log.create(dataLog);
  }

  return endpointResult;
}

routes.post("/api/bug-report", (req, res) => {
  if (process.env.DISCORD_WEBHOOK) {
    axios
      .post(process.env.DISCORD_WEBHOOK, { content: req.body.data })
      .then(() => {
        res.send("Discord message sent");
      })
      .catch((e) => {
        // console.error(e);
        res.send(e.message);
      });
  } else {
    return res.status(204).send("Discord webhook not set");
  }
});

sessionController.store.name = "sessionControllerStore";
routes.post("/api/sessions", (req, res, next) =>
  logEndpoint({ req, res, next }, sessionController.store)
);
routes.post("/api/sendPasswordResetMail", (req, res, next) =>
  logEndpoint({ req, res, next }, sessionController.sendPasswordResetMail)
);
routes.post("/api/setPassword", (req, res, next) =>
  logEndpoint({ req, res, next }, sessionController.setPassword)
);

authMiddleware.name = "authMiddleware";
routes.use((req, res, next) => logEndpoint({ req, res, next }, authMiddleware));

routes.get("/api/metadata", (req, res) =>
  logEndpoint({ req, res }, metadataController.getMeta)
);

routes.get("/api", (req, res) => {
  return res.status(200).send({ message: "Welcome to the Supernova!" });
});

routes.post("/api/changeOrganization", (req, res, next) =>
  logEndpoint({ req, res, next }, sessionController.changeOrganization)
);

routes.get("/api/data", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.browse)
);
routes.post("/api/data", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.store)
);
routes.get("/api/data/:id", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.show)
);
routes.put("/api/data/:id", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.update)
);
routes.delete("/api/data/:id", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.delete)
);

/* V2 - Mass data */ 
routes.post("/api/v2/data", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.createV2)
);
routes.delete("/api/v2/data", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.deleteV2)
);
routes.put("/api/v2/data", (req, res, next) =>
  logEndpoint({ req, res, next }, magicController.updateV2)
);

routes.post("/api/upload", upload.array("data"), async (req, res) => {
  const bucketName = process.env.GOOGLE_STORAGE_BUCKET;

  const user = Auth.user();
  const OrganizationId = req.get("Organization");

  console.log(user.id, OrganizationId);
  const files = await File.bulkCreate(
    req.files.map((file) => {
      return {
        CreatedByUserId: user.id,
        OrganizationId,
        label: file.originalname,
        mimetype: file.mimetype,
        size: file.size,
      };
    }),
    { returning: ["id"] }
  );

  await Promise.all(
    req.files.map(async (file, index) => {
      const folder = files[index].id;
      const destination = `${folder}/${file.originalname}`;

      if (storage) {
        // upload file
        await storage.bucket(bucketName).upload(file.path, {
          destination,
        });

        // make it public
        return storage.bucket(bucketName).file(destination).makePublic();
      } else {
        const filePath = path.resolve(diskStoragePath, folder);
        await fs.mkdir(filePath);
        await fs.rename(
          path.resolve(__dirname, file.path),
          path.resolve(diskStoragePath, destination)
        );
      }
    })
  );

  return res.send({ result: files.map((i) => i.id) });
});

routes.get("/api/reports", (req, res, next) =>
  logEndpoint({ req, res, next }, reportController.getAvailableReports)
);
routes.get("/api/reports/:id", (req, res, next) =>
  logEndpoint({ req, res, next }, reportController.runReport)
);

module.exports = routes;
