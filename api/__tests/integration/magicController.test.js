const test = require('ava');
const request = require('supertest');

const app = require('../../src/app');
const factory = require('../factories');

test('Should return error when trying to access data without specifing a target model', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .get('/api/data')
    .set('Authorization', `Bearer ${user.generateToken()}`)
    .send();

  t.is(response.status, 400)
  t.true(Array.isArray(response.body.messages))
});

test('Should return the models SELECT result when a target model is specified', async t => {
  const user = await factory.create('User', { password: '1234' });

  const response = await request(app)
    .get('/api/data')
    .set('Authorization', `Bearer ${user.generateToken()}`)
    .set('Model', 'User')
    .send();

  t.is(response.status, 200)
  t.true(Array.isArray(response.body.result))
});

